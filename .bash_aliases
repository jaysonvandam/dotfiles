alias v="vim"
alias ltx="pdflatex"
# Replace this with whatever the current sound device is
alias suspend="sudo systemctl suspend"
# "youtube music" ...downloads the best quality audio file from a youtube video
alias ytm="youtube-dl -i --extract-audio --audio-format mp3 --audio-quality 0"
alias up="sudo apt update; sudo apt full-upgrade -y"
alias au="sudo apt update"
alias afu="sudo apt full-upgrade"
alias ec="echo 'terminal width = $COLUMNS'"
alias dm1="crispy-doom -iwad ~/games/DOOM.WAD"
alias dm2="crispy-doom -iwad ~/games/DOOM2.WAD"
