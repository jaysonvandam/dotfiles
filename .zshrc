# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/jace/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# default "redhat" zsh prompt
autoload -Uz promptinit
promptinit
prompt redhat

# my custom zsh prompt
#PROMPT='%B%F{green}%n@%m%f:%F{blue}%~%b%f$ '

# aliases from bashrc
alias ls='ls --color=auto' # easier fancy colors in ls
# print ls in column format
alias lw='ls --format=single-column'
alias grep='grep --color=auto' # easier fancy colors in grep
alias mv="mv -i" # ensures I don't overwrite something I didn't want to.
alias rm="rm -i" # ensures I don't remove    something I didn't want to.
alias cp="cp -i" # ensures I don't copy      something I didn't want to.
alias p="sudo pacman"
alias v="vim"
alias sv="sudo vim"
alias ltx="pdflatex"
# "youtube music" ...downloads the best quality audio file from a youtube video
alias ytm="youtube-dl -i --extract-audio --audio-format mp3 --audio-quality 0"
