# These are my dotfiles for GNU/Linux

As of writing, I'm currently using Kubuntu, but these should work
for any GNU/Linux distro.

The `_vimrc` file is for if you have a Vim installation on Windows. Place that in the
`C:\Users:[username]\` directory
